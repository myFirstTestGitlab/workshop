<img src="elixir-norway.png" width="150">  <img src="nmbu.png" width="200"> 

## Workshop on using High-Performance Cluster and ELIXIR tools for Bioinformatics 

###[Program and registration](program)

###Morning session
*For Windows users: If you haven't do that, please install an SSH client such as [MobaXterm](https://mobaxterm.mobatek.net/download-home-edition.html) (download the "Portable edition") or [PuTTY](http://www.putty.org/).*

- [Slides](your_link_to_slides)


###Afternoon session
*For those who has never used NeLS before, please log in to [NeLS](https://nels.bioinfo.no/) using your FEIDE id so that your NeLS profile would be automatically created.*

- [Introduction to NeLS and Galaxy](your_link_to_slides)

- [Galaxy workflow](ATAC-seq-galaxy-workflow.pptx)

- [An example of ATAC-seq workflow](https://drive.google.com/file/d/1-wGC84rs8eue1JboS_1LOB2pSaBaLe4m/view?usp=sharing)


