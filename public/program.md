
## Workshop on using High-Performance Cluster and ELIXIR tools for Bioinformatics 

__Date: 30th October 2019__

__Venue: Room U122, Urbygningen, NMBU Ås__

This is a 1-day workshop split in a morning and an afternoon session. You can register to one or both sessions.  The first session is dedicated to those who would like to use command line and High-Performance Cluster to handle their research data. The second session provides some introduction to storing, sharing, and processing data with bioinformatic tools, using Galaxy and NeLS as web-interface. 

Participants should bring their own laptops. No prior experience is required for any of the sessions.

### Morning Session (10:30-12:00):
*Introduction to UNIX & High-Performance Cluster (HPC).*

Using command line and HPC is a practical and efficient way for processing massive data. You will learn how to access an HPC (exemplified by the NMBU computer cluster Orion), how to manage your data and how to use the HPC to run your analysis. This session requires a laptop with a UNIX terminal. Detailed setup information about this will be sent out later. 

- The UNIX terminal
- Introduction to HPC
- Writing and submitting scripts

### Lunch (12:00-12:45): 
*A coffee bar and a canteen is available close to the venue, or you can bring your “matpakke”.*

### Afternoon Session (12:45-15:45):
*Introduction to NeLS and Galaxy.*

We will present the Norwegian e-Infrastructure for Life Sciences (NeLS), a users-friendly solution for storing and sharing life sciences data. NeLS is integrated with Galaxy, an open source, web-based platform for bioinformatics pipelines. This session will cover how to access NeLS, to request storage for your projects, to share your data, and to use Galaxy for analyzing data stored in NeLS or in other sources. All steps will be made through user-friendly web-interfaces, no software will be required for this session.

-	Introduction to NeLS/Galaxy
-	Hands-on practice on running Galaxy and making a pipeline 
-	An example of an ATAC-seq pipeline

#### Registrations should be made through: https://form.nmbu.no/view.php?id=550855 

There are limited number of places, and registration will be granted on a first-come-first-served basis.

This workshop is run by ELIXIR NMBU. 
Organizers & trainers for this event include: Torfinn Nome (torfinn.nome@nmbu.no), Amine Namouchi (amine.namouchi@nmbu.no), Thu-Hien To (thu-hien.to@nmbu.no) and Dag Inge Våge. If you have any questions, don’t hesitate to contact us.   


